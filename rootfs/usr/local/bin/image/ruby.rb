# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Ruby
      module Build
        def self.main
          System.run('apk add --no-cache ruby-full')
        end
      end

      module Run
        def self.main
          Shell.update_user
          System.execute('irb', sudo: USER)
        end
      end
    end
  end
end
